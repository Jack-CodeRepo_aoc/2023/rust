

use std::fs;
use std::io::{BufRead, BufReader};
use regex::Regex;

fn file_as_list(file_path: &str) -> Vec<String> {
    let input = fs::File::open(file_path).expect(&*format!("Unable to read file : {}", file_path));

    let buf = BufReader::new(input);
    buf.lines().map(|l| l.expect("Could not parse line")).collect()

}


fn first_part(input: Vec<String>){
    let mut r = vec![];
    for ligne in &input {
        let mut tmp = vec![];
        for c in ligne.as_str().chars() {
            if (c.is_digit(10)) { tmp.push(c)}
        }
        r.push(tmp);
    }
    let mut result = 0;
    for l in r {
        result += format!("{}{}",l[0].to_string(), l[l.len() - 1].to_string()).parse::<i32>().unwrap();
    }
    println!("{}",result);



}


fn main() {

    let input = file_as_list("input.txt");

    first_part(input);



}
